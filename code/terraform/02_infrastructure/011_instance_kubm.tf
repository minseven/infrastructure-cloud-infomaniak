
module "kubm" {
  source                      = "../modules/instance_v2"
  instance_count              = 3
  instance_name               = "kubm"
  instance_key_pair           = "default_key"
  instance_security_groups    = ["consul", "ssh-internal", "all_internal", "node_exporter"]
  instance_network_internal   = var.network_internal_dev
  instance_flavor_name        = "a2-ram4-disk50-perf1"
  instance_ssh_key = var.ssh_public_key_default_user
  public_floating_ip = false
  instance_default_user = var.default_user
  instance_internal_fixed_ip = "10.0.1.3"
  allowed_addresses           = ["10.200.0.0/16","10.201.0.0/16"]
  metadatas                   = {
    environment          = "dev",
    app         = "kubm"
  }
}

