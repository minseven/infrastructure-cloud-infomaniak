resource "openstack_networking_router_v2" "rt1" {
  name                = "rt1"
  admin_state_up      = "true"
  external_network_id = var.network_external_id
}

module "network_dev" {
  source          = "../modules/network"
  network_name		= var.network_internal_dev
  network_subnet_cidr	= var.network_subnet_cidr
  router_id		= openstack_networking_router_v2.rt1.id
}


resource "openstack_networking_router_route_v2" "router_route_1" {
  router_id        = openstack_networking_router_v2.rt1.id
  destination_cidr = "10.203.0.0/16"
  next_hop         = "10.0.1.31"
}