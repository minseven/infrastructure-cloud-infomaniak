%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Loki : installation & role ansible

<br>

Installation :

    * create an instance with a volume

    * create an ansible role

    * install grafana apt repository

    * install loki

    * install data directory

    * add configuration file

-----------------------------------------------------------------------------------------------------------                                       

# Loki : installation & role ansible

<br>

Instanciate a module :

```
module "loki" {
  source                      = "../modules/instance"
  instance_count              = 1
  instance_name               = "loki"
  instance_key_pair           = "default_key"
  instance_security_groups    = ["consul", "ssh-internal", "all_internal", "node_exporter"]
  instance_network_internal   = var.network_internal_dev
  instance_ssh_key = var.ssh_public_key_default_user
  instance_default_user = var.default_user
  instance_volumes_count = 1  
  metadatas                   = {
    environment          = "dev",
    app         = "loki"
  }
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Loki : installation & role ansible

<br>

Install grafana repository

```
- name: add gpg hey
  apt_key:
    url: "https://packages.grafana.com/gpg.key"
    validate_certs: no

- name: add repository
  apt_repository:
    repo: "deb https://packages.grafana.com/oss/deb stable main"
    state: present
    validate_certs: no
```

-----------------------------------------------------------------------------------------------------------                                       

# Loki : installation & role ansible

<br>

Install Loki

```
- name: install loki
  apt:
    name: loki
    state: latest
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------                                       

# Loki : installation & role ansible

<br>

Create data dedicated directory

```
- name: create data dir for loki
  file:
    state: directory
    path: "{{ loki_dir_data }}"
    owner: loki
    group: root
    mode: 0755
    recurse: yes
```

```
loki_dir_data: "/data/loki"
```

-----------------------------------------------------------------------------------------------------------                                       

# Loki : installation & role ansible

<br>

Change the configuration file

```
- name: loki install configuration
  template:
    src: config.yml.j2
    dest: /etc/loki/config.yml        
    mode: 0755
  notify: restart_loki

- name: start loki
  systemd:
    name: loki
    state: started
    enabled: yes 
```

-----------------------------------------------------------------------------------------------------------                                       

# Loki : installation & role ansible

<br>

Handler

```
- name: restart_loki
  systemd:
    name: loki
    state: restarted
    enabled: yes   
```


-----------------------------------------------------------------------------------------------------------                                       

# Loki : installation & role ansible

<br>

Handler

```
auth_enabled: false
server:
  http_listen_port: 3100
  grpc_listen_port: 9096
common:
  instance_addr: 0.0.0.0
  path_prefix: {{ loki_dir_data }}
  storage:
    filesystem:
      chunks_directory: {{ loki_dir_data }}/chunks
      rules_directory: {{ loki_dir_data }}/rules
  replication_factor: 1
  ring:
    kvstore:
      store: inmemory
query_range:
  results_cache:
    cache:
      embedded_cache:
        enabled: true
        max_size_mb: 100
  parallelise_shardable_queries: false
schema_config:
  configs:
    - from: 2020-10-24
      store: boltdb-shipper
      object_store: filesystem
      schema: v11
      index:
        prefix: index_
        period: 24h
```

-----------------------------------------------------------------------------------------------------------                                       

# Loki : installation & role ansible

<br>

Playbook

```
- name: logging
  become: yes
  hosts: meta-app_consul
  roles:
    - base/volumes
    - observability/loki
    - consul/consul_services
  vars:
    volumes_disks:
      - {disk: '/dev/sdb',path: '/data', owner: "consul"}
    consul_services:
      - { 
        name: "loki",
        type: "http",
        target: "http://127.0.0.1:3100/metrics", 
        interval: "10s", 
        port: 3100
        }
```