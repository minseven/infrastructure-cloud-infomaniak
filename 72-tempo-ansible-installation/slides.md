%title: Infra Cloud Infomaniak
%author: xavki


████████╗███████╗███╗   ███╗██████╗  ██████╗ 
╚══██╔══╝██╔════╝████╗ ████║██╔══██╗██╔═══██╗
   ██║   █████╗  ██╔████╔██║██████╔╝██║   ██║
   ██║   ██╔══╝  ██║╚██╔╝██║██╔═══╝ ██║   ██║
   ██║   ███████╗██║ ╚═╝ ██║██║     ╚██████╔╝
   ╚═╝   ╚══════╝╚═╝     ╚═╝╚═╝      ╚═════╝ 


-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

Create a new role

Better to use S3/Minio...

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

And a new instance

```
module "tracing" {
  source                      = "../modules/instance"
  instance_count              = 1
  instance_name               = "tracing"
  instance_key_pair           = "default_key"
  instance_security_groups    = ["consul", "ssh-internal", "all_internal", "node_exporter"]
  instance_network_internal   = var.network_internal_dev
  instance_ssh_key = var.ssh_public_key_default_user
  instance_default_user = var.default_user
  instance_volumes_count = 1
  metadatas                   = {
    environment          = "dev",
    app         = "tracing"
  }
}
```

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

<br>

Some variables

```
tempo_version: 2.3.0
tempo_dir_config: /etc/tempo
tempo_dir_data: ""
tempo_dir_binary: /usr/local/bin/
tempo_remote_write_metrics: ""
```

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

Install utils if needed

```
- name: install utils
  apt:
    name: 
     - curl
     - wget
     - unzip
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

Check if tempo already install

```
- name: check if tempo exists
  stat:
    path: /usr/local/bin/tempo
  register: __tempo_exists
```

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

Check tempo version

```  
- name: if tempo exists get version
  shell: "cat /etc/systemd/system/tempo.service | grep Version | sed s/'.*Version '//g"
  register: __get_tempo_version
  when: __tempo_exists.stat.exists == true
  changed_when: false
```

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

Creattempo directories

```
- name: create directory for tempo configuration
  file:
    path: "{{ item }}"
    state: directory
    mode: 0750
  loop:
  - "{{ tempo_dir_config }}"
  - "{{ tempo_dir_data }}"
```

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

Install binary

```
- name: download tempo
  unarchive: 
    src: "https://github.com/grafana/tempo/releases/download/v{{ tempo_version }}/tempo_{{ tempo_version }}_linux_amd64.tar.gz"
    dest: /tmp/
    remote_src: yes
  when: __tempo_exists.stat.exists == False or not __get_tempo_version.stdout == tempo_version
```

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

Install binary

```
- name: move the binary to the final destination
  copy:
    src: "/tmp/{{ item }}"
    dest: "{{ tempo_dir_binary }}/{{ item }}"
    mode: 0750
    remote_src: yes
  when: __tempo_exists.stat.exists == False or not __get_tempo_version.stdout == tempo_version
  loop:
  - tempo
  - tempo-cli
  - tempo-query
```

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

Install the systemd service

```
- name: tempo systemd file
  template:
    src: "tempo.service.j2"
    dest: "/etc/systemd/system/tempo.service"         
    mode: 0750
  notify: "restart_tempo"
```

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

Add configuration file

```
- name: tempo configuration file
  template:
    src: "tempo.yml.j2"
    dest: "{{ tempo_dir_config }}/tempo.yaml"      
    mode: 0750
  notify: "restart_tempo"
```

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

Add configuration file

```
- meta: flush_handlers

- name: start tempo
  systemd:
    name: tempo
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

Add handler

```
- name: restart_tempo
  systemd:
    name: tempo
    state: restarted
    enabled: yes
    daemon_reload: yes
```

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

The systemd service

```
[Unit]
Description=tempo Version  {{ tempo_version }}
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
WorkingDirectory={{ tempo_dir_config }}
ExecStart=/usr/local/bin/tempo -config.file={{ tempo_dir_config }}/tempo.yaml

[Install]
WantedBy=multi-user.target
```

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

The playbook

```
- name: tracing
  become: yes
  hosts: meta-app_tracing
  roles:
    - base/volumes
    - observability/tempo
    - consul/consul_services
  vars:
    tempo_remote_write_metrics: "http://victoriametrics.service.xavki.consul:8428/api/v1/write"
    tempo_dir_data: "/data/tempo"
    volumes_disks:
      - {disk: '/dev/sdb',path: '/data'}
    consul_services:
      - { 
        name: "tempo",
        type: "http",
        target: "http://127.0.0.1:3200/metrics", 
        interval: "10s", 
        port: 3100
        }
```

-----------------------------------------------------------------------------------------------------------

# Tempo : Ansible & Installation

<br>

Change traefikk configuration

```
[tracing]
  [tracing.jaeger.collector]
    endpoint = "http://{{ traefik_tracing_jaeger }}:14268/api/traces?format=jaeger.thrift"
```
