%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMalert & ansible

<br>

Vmalert

  * playlist on the xavki channel

  * same settings as prometheus rules

  * doc : https://docs.victoriametrics.com/vmalert/

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMalert & ansible

<br>

Some variables

```
vmalert_etc_config: "/etc/vmalert"
vmalert_remotewrite: "127.0.0.1:8428"
vmalert_remote: vmetrics.service.xavki.consul
vmalert_grafana: grafana.service.xavki.consul
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMalert & ansible

<br>

Reuse vmagent role = vmtools

```
- name: create directory for vmagent configuration
  file:
    path: "{{ item }}"
    state: directory
    mode: 0750
  loop:
  - "{{ vmagent_etc_config }}"
  - "{{ vmalert_etc_config }}"
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMalert & ansible

<br>

Use a dedicated directory in templates with a fileglob

```
- name: push alerts configuration
  template:
    src: "{{ item }}"
    dest: "/etc/vmalert/{{ item }}"
    mode: 0750
  with_fileglob:
  - templates/alerts/*.yml
  notify: restart_vmalert
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMalert & ansible

<br>

Start with a loop

```
- name: start vmagent
  systemd:
    name: "{{ item }}"
    state: started
    enabled: yes
  loop:
  - vmagent
  - vmalert
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMalert & ansible

<br>

Add the handler

```
- name: restart_vmalert
  systemd:
    name: vmagent
    state: restarted
    enabled: yes
    daemon_reload: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMalert & ansible

<br>

Add the systemd service

```
[Unit]
Description=Description=VictoriaAlert service Version {{ vmagent_version }}
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/bin/vmalert-prod \
      -datasource.url=http://{{ vmalert_remote }}:8428/ \
      -remoteRead.url=http://{{ vmalert_remote }}:8428/ \
      -remoteWrite.url=http://{{ vmalert_remote }}:8428/ \
      -notifier.url=http://127.0.0.1:9093 \
      -rule=/etc/vmalert/*.yml \
      -httpListenAddr=http://0.0.0.0:8430 \
      -external.url=http://{{ vmalert_grafana }}:3000

SyslogIdentifier=vmagent
Restart=always

[Install]
WantedBy=multi-user.target
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMalert & ansible

<br>

Some sources of alerts

https://samber.github.io/awesome-prometheus-alerts/rules.html
https://samber.github.io/awesome-prometheus-alerts/rules.html#host-and-hardware-1

https://github.com/samber/awesome-prometheus-alerts

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - VMalert & ansible

<br>

Add the systemd service

```
groups:
  - name: test
    rules:
    - alert: Trop_2_load
      expr: node_load1{instance!~"prom1.*"} >= 0.6
      for: 10s
      labels:
        severity: critical
      annotations:
        summary: "{{ $labels.instance }} - trop de load"
        description: "Le server en prend plein la tronche "
```

