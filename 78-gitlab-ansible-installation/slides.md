%title: Infra Cloud Infomaniak
%author: xavki


 ██████╗ ██╗████████╗██╗      █████╗ ██████╗ 
██╔════╝ ██║╚══██╔══╝██║     ██╔══██╗██╔══██╗
██║  ███╗██║   ██║   ██║     ███████║██████╔╝
██║   ██║██║   ██║   ██║     ██╔══██║██╔══██╗
╚██████╔╝██║   ██║   ███████╗██║  ██║██████╔╝
 ╚═════╝ ╚═╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚═════╝ 



-----------------------------------------------------------------------------------------------------------

# Gitlab : Ansible installation

<br>

Some variables

```
gitlab_version: 17.0.0-ce.0
gitlab_external_url: "" #http://gitlab.xavki.fr
gitlab_listen_https: "false"
gitlab_listen_port: 80
gitlab_data_dirs: "" # /data/gitlab/
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Ansible installation

<br>

Tasks - prerequisites

```
- name: install utils
  apt:
    name: gnupg2,software-properties-common,postfix,ca-certificates
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Ansible installation

<br>

Tasks - add gpg key

```
- name: add gpg key
  apt_key:
    url: "http://packages.gitlab.com/gitlab/gitlab-ce/gpgkey"
    validate_certs: no
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Ansible installation

<br>

Tasks - add repository

```
- name: add repository
  apt_repository:
    repo: "deb http://packages.gitlab.com/gitlab/gitlab-ce/debian bookworm main"
    state: present
    validate_certs: no
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Ansible installation

<br>

Tasks - install gitlab

```
- name: install gitlab
  apt:
    name: gitlab-ce={{ gitlab_version }}
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Ansible installation

<br>

Tasks - change configuration

```
- name: install configuration file
  template:
    src: gitlab.rb.j2
    dest: /etc/gitlab/gitlab.rb
    owner: root
    group: root
    mode: 0600
  notify: reconfigure_gitlab
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Ansible installation

<br>

Tasks - change configuration

```
- name: Add datadir if needed
  ansible.builtin.blockinfile:
    path: /etc/gitlab/gitlab.rb
    insertafter: ".*For setting up different data storing directory.*"
    block: |
      git_data_dirs({
        "default" => {
          "path" => "{{ gitlab_data_dirs }}"
         }
      })
  when: gitlab_data_dirs != ""
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Ansible installation

<br>

Tasks - change configuration

```
- name: change listen port
  lineinfile:
    dest: /etc/gitlab/gitlab.rb
    insertafter: ".*nginx['listen_port'] = nil"
    line: "nginx['listen_port'] = {{ gitlab_listen_port }}"
  when: gitlab_listen_port != ""
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Ansible installation

<br>

Tasks - change configuration

```
- name: change listen port
  lineinfile:
    dest: /etc/gitlab/gitlab.rb
    insertafter: ".*nginx['listen_https'] = nil"
    line: "nginx['listen_https'] = {{ gitlab_listen_https }}"
  when: gitlab_listen_https != ""
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Ansible installation

<br>

Tasks - change configuration

```
- name: change external url
  lineinfile:
    dest: /etc/gitlab/gitlab.rb
    regex: "external_url.*"
    line: 'external_url "{{ gitlab_external_url }}"'
  when: gitlab_external_url != ""
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Ansible installation

<br>

Handler

```
- name: reconfigure_gitlab
  shell:
    cmd: gitlab-ctl reconfigure
  args:
    executable: "/bin/bash"
  changed_when: false
  failed_when: false
  async: 600
  poll: 10
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Ansible installation

<br>

The playbook - warn ssh connection

```
- name: gitlab
  become: yes
  hosts: all
  roles:
    - base/volumes
    - gitlab
    - consul/consul_services
  vars:
    volumes_disks:
      - {disk: '/dev/sdb',path: '/var/opt/gitlab'}
    consul_services:
      - { 
        name: "gitlab", 
        type: "http", 
        target: "http://127.0.0.1:80", 
        interval: "10s", 
        port: 80,
        tags: [
            "traefik.enable=true",
            "traefik.http.routers.router-gitlab.entrypoints=http,https",
            "traefik.http.routers.router-gitlab.rule=Host(`gitlab.xavki.fr`)",
            "traefik.http.routers.router-gitlab.service=gitlab",
            "traefik.http.routers.router-gitlab.middlewares=auth@file,to_https@file,secure_headers@file",
            "traefik.http.routers.router-gitlab.tls.certresolver=xavki_certs"
          ] }
      - { 
        name: "gitlab-ssh", 
        type: "tcp", 
        target: "127.0.0.1:22", 
        interval: "10s", 
        port: 22,
        tags: [
            "traefik.enable=true",
            "traefik.tcp.routers.router-gitlab-ssh.rule=HostSNI(`*`)",
            "traefik.tcp.routers.router-gitlab-ssh.entrypoints=ssh",
            "traefik.tcp.routers.router-gitlab-ssh.service=gitlab-ssh"
          ] }
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Ansible installation

<br>

Traefik configuration change - warn ssh connection

```
  [entryPoints.ssh]
    address = ":2222"
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Ansible installation

<br>

Test

```
ssh xavki@gl.xavki.fr -p 2222

Host gl.xavki.fr
  port 2222
```